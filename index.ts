import axios, {AxiosResponse} from 'axios';
import {readFileSync} from 'fs';
import commander = require('commander');

interface INewReceiptRequest {
    amount: string;
    date: string;
    time: string;
    fik: string;
    simpleMode: boolean;
    phone: string;
}

interface ILoginRequest {
    username: string;
    password: string;
}

interface IServerError {
    field: string;
    code: string;
    message: string;
}

interface ILoginResponse {
    accessToken: string;
    refreshToken: string;
}

interface IReceipt {
    dateTime: Date;
    amount: number;
    fik: string;
    simpleMode?: boolean;
    phone?: string;
}

function zeroStart(str: string, length: number): string {
    let newStr = str;

    if(newStr.length < length) {
        newStr = '0'.repeat(length - newStr.length) + newStr;
    }

    return newStr;
}

async function registerNewReceipt(accessToken: string, receipt: IReceipt): Promise<undefined> {
    const request: INewReceiptRequest = {
        amount: receipt.amount.toFixed(2).replace(/[^0-9]/g, ''),
        date: `${receipt.dateTime.getFullYear()}-${zeroStart((receipt.dateTime.getMonth() + 1).toString(), 2)}-${zeroStart(receipt.dateTime.getDate().toString(), 2)}`,
        time: `${zeroStart(receipt.dateTime.getHours().toString(), 2)}:${zeroStart(receipt.dateTime.getMinutes().toString(), 2)}`,
        fik: receipt.fik,
        simpleMode: receipt.simpleMode !== undefined ? receipt.simpleMode : false,
        phone: receipt.phone !== undefined ? receipt.phone : null
    };


    try {
        await axios.post('https://www.uctenkovka.cz/api/web/player/receipts/', request, {
            headers: {
                Authorization: `Bearer ${accessToken}`,
                'Content-Type': 'application/json;charset=UTF-8',
            }
        });
        return;
    } catch(err) {
        if(err.response) {
            const responses = <IServerError[]>err.response.data;
            if(responses.length > 0) {
                throw new Error(`Error for receipt ${receipt.fik}: ${responses.map((response) => response.message).join(',')}`);
            }
        } else {
            throw err;
        }
    }
}

async function getAccessToken(username: string, password: string): Promise<string> {
    const request: ILoginRequest = {
        username,
        password
    };

    const result = await axios.post('https://www.uctenkovka.cz/api/web/auth/login', request, {
        headers: {
            'Content-Type': 'application/json; charset=UTF-8',
        }
    });

    return (<ILoginResponse>result.data).accessToken;
}

function parseReciept(line: string): IReceipt {
    // 5980/03112018/1237/9cb8e736-1cd3-4b27
    const parts = line.split('/');
    if(parts.length !== 4) {
        throw new Error(`Line '${line}' doesn't meet format`);
    }

    const amountMatches = parts[0].match(/^(\d+)(\d{2})$/);
    if(!amountMatches) {
        throw new Error(`Error occurred to extract amount from '${line}'`);
    }
    const amount = parseInt(amountMatches[1]) + parseInt(amountMatches[2]) / 100.0;

    const dateMatches = parts[1].match(/^(\d{2})(\d{2})(\d{4})$/);
    const timeMatches = parts[2].match(/^(\d{2})(\d{2})$/);
    if(!dateMatches || !timeMatches) {
        throw new Error(`Error occurred to extract date and time from '${line}'`);
    }
    const dateTime = new Date(
        parseInt(dateMatches[3]),
        parseInt(dateMatches[2]) - 1,
        parseInt(dateMatches[1]),
        parseInt(timeMatches[1]),
        parseInt(timeMatches[2])
    );

    const fik = parts[3];

    return {
        fik,
        amount,
        dateTime
    };
}

async function launch() {
    commander
        .version('1.0.0')
        .usage('-f ./uctenky.cz -t <JWT_TOKEN>')
        .option('-p --password <password>', 'Password')
        .option('-u --username <username>', 'Email or phone number')
        .option('-f --file <file>', 'File with receipts')
        .parse(process.argv);

    const accessToken = await getAccessToken(commander.username, commander.password);

    const lines = readFileSync(commander.file, 'utf-8').split('\n');
    for(let index = 0; index < lines.length; index++) {
    // for(let index = 0; index < 1; index++) {
        const line = lines[index];

        try {
            const receipt = parseReciept(line);
            await registerNewReceipt(accessToken, receipt);

            console.log(`Successfully registered ${receipt.fik} in Uctenkovka.cz`);
        } catch(err) {
            console.error(err.message);
        }
    }
}

launch();